package id

import (
	"github.com/pkg/errors"
	"strconv"
)

type converterUint64 struct{}

func (c *converterUint64) FromString(s string) (*ID, error) {
	if s == "" {
		return nil, errors.New("can't be empty")
	}
	value, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		return nil, errors.Errorf(`bad uint64 string: "%s"`, s)
	}
	i := New()
	i.Value = value
	return i, nil
}

func (c *converterUint64) ToString(id *ID) string {
	return strconv.FormatUint(id.Value.(uint64), 10)
}

var ConverterUint64 = &converterUint64{}
