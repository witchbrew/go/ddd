package id

import (
	"github.com/stretchr/testify/require"
	"math"
	"strconv"
	"testing"
)

func TestConverterUint64_FromString_MaxUint64(t *testing.T) {
	i, err := ConverterUint64.FromString(strconv.FormatUint(math.MaxUint64, 10))
	require.Nil(t, err)
	require.Equal(t, uint64(math.MaxUint64), i.Value)
}

func TestConverterUint64_FromString_Empty(t *testing.T) {
	_, err := ConverterUint64.FromString("")
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "can't be empty")
}

func TestConverterUint64_FromString_RandomString(t *testing.T) {
	_, err := ConverterUint64.FromString("hello")
	require.NotNil(t, err)
	require.Contains(t, err.Error(), `bad uint64 string: "hello"`)
}

func TestConverterUint64_FromString_Negative(t *testing.T) {
	_, err := ConverterUint64.FromString("-5")
	require.NotNil(t, err)
	require.Contains(t, err.Error(), `bad uint64 string: "-5"`)
}

func TestConverterUint64_String_Decimal(t *testing.T) {
	_, err := ConverterUint64.FromString("1.5")
	require.NotNil(t, err)
	require.Contains(t, err.Error(), `bad uint64 string: "1.5"`)
}

func TestConverterUint64_ToString_MaxUint64(t *testing.T) {
	maxUint64 := uint64(math.MaxUint64)
	id := New()
	id.Value = maxUint64
	require.Equal(t, strconv.FormatUint(maxUint64, 10), ConverterUint64.ToString(id))
}